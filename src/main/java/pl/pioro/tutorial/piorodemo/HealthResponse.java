package pl.pioro.tutorial.piorodemo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter @Setter @NoArgsConstructor
public class HealthResponse {

	String buildVersion, buildTime, uptime;
	Date startTime;
	String[] activeProfiles;
}
