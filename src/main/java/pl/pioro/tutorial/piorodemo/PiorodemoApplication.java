package pl.pioro.tutorial.piorodemo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.info.BuildProperties;

@SpringBootApplication
public class PiorodemoApplication {
	@Autowired
	private BuildProperties buildProperties;
	
	
	public static void main(String[] args) {
		SpringApplication.run(PiorodemoApplication.class, args);
	}
	
	 @PostConstruct
	    private void logVersion() {
		 	System.out.println(buildProperties.getVersion() + " - "+buildProperties.getTime());
//	        if(buildProperties.isPresent()) {
//	        LOGGER.info("### BUILD_INFO=[version={}, buildTime={}]",buildProperties.get().getVersion(),buildProperties.get().getTime());
//	        }
	    }

}
