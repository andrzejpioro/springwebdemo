package pl.pioro.tutorial.piorodemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Autowired
	private BuildProperties buildProperties;

	@Autowired
	private HealthService healthService;
	
	@Autowired
	private Environment environment;

	

	@GetMapping("/info")
	public HealthResponse version() {
		HealthResponse resp = new HealthResponse();
		resp.setBuildVersion(this.buildProperties.getVersion());
		resp.setBuildTime(this.buildProperties.getTime().toString());
		resp.setUptime(this.healthService.getUptimeInSec()+ " s");
		resp.setActiveProfiles(this.environment.getActiveProfiles());
		resp.setStartTime(this.healthService.getStartUpTime());
		return resp;
	}
	
	
	

	@RequestMapping("/")
	public String index() {
		return "Greetings from Pioro App!";
	}

}