package pl.pioro.tutorial.piorodemo;

import java.util.Date;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
public class HealthService {
	@Getter
	private Date startUpTime;
	
	
	
	public HealthService() {
		this.startUpTime=new Date();
	}
	
	public long getUptimeInSec() {
		return (new Date().getTime()-this.startUpTime.getTime())/1000;
	}


}
